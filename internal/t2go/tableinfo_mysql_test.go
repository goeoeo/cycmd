package t2go

import (
	"gitee.com/phpdi/cycmd/internal/util"
	"log"
	"testing"
)

func Test_mysqlTableInfo_GetTableInfo(t *testing.T) {
	ti := NewMysqlTableInfo("root:2DnqzOVB0WGN@(139.198.123.116:33306)/goadmin?charset=utf8")
	info, err := ti.GetTableInfo("exp_commission_trans")
	if err != nil {
		log.Fatal(err)
	}
	util.PrintJson(info)
}
