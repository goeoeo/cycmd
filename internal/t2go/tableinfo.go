package t2go

type (
	TableInfo interface {
		GetTableInfo(tableName string) (Info, error)
	}

	Info struct {
		Name      string     //表名
		Comment   string     //表注释
		Columns   []Column   //字段信息
		UniqueKey [][]string //唯一字段
	}

	Column struct {
		Field      string //字段名
		GoType     string //字段类型
		SourceType string //源数据库字段类型
		PK         bool   //是否为主键
		Comment    string //字段注释
		GormTag    string //gormTag
	}
)
