package models

import (
	"gorm.io/gorm"
	"time"
)

type {{.EntityName}} struct {
    {{RenderingField .PkField true true}}
    {{range .Fields}}{{RenderingField . true true}}
    {{end}}{{range .TimeField}}{{RenderingField . true true}}
    {{end}}
}

func (*{{.EntityName}}) TableName() string {
	return "{{.TableName}}"
}

func (s *{{.EntityName}}) BeforeCreate(tx *gorm.DB) (err error) {
    {{range .UniqueKey}}
    if err = db.Repeat(tx, s, "{{Join . }}", "{{Join . }}重复"); err != nil {
    	return
    }
    {{end}}

	return
}

func (s *{{.EntityName}}) BeforeUpdate(tx *gorm.DB) (err error) {
    {{range .UniqueKey}}
    if err = db.Repeat(tx, s, "{{Join .}}", "{{Join . }}重复"); err != nil {
    	return
    }
    {{end}}
	return
}
