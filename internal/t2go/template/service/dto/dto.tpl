package dto

import (
	models2 "gitee.com/zhangfei_/city-express-api/app/admin/models"
	"gitee.com/zhangfei_/city-express-api/common/dto"
)

type (
	Create{{.EntityName}}Req struct {
        {{range .Fields}}{{RenderingField . true false}}
        {{end}}
	}
	Create{{.EntityName}}Ack struct {
	}
)

type (
	Update{{.EntityName}}Req struct {
	    {{RenderingField .PkField true false}}
        {{range .Fields}}{{RenderingField . true false}}
        {{end}}
	}
	Update{{.EntityName}}Ack struct {
	}
)

type (
	Delete{{.EntityName}}Req struct {
		{{RenderingField .PkField true false}}
		SysUserId int `json:"sys_user_id" ` //用户ID
	}
	Delete{{.EntityName}}Ack struct {
	}
)

type (
	Get{{.EntityName}}Req struct {
		Id        int64 `uri:"id"`
		SysUserId int `json:"sys_user_id" ` //用户ID
	}
)

type (
	Page{{.EntityName}}Req struct {
		dto.Pagination `search:"-"`
        SysUserId      int `json:"sys_user_id" search:"-"` //用户ID
	}
	Page{{.EntityName}}Ack struct {
		List []*models2.{{.EntityName}}
		Count int64
	}
)
