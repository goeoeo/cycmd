package apis

import (
	"gitee.com/zhangfei_/city-express-api/app/admin/models"
	"gitee.com/zhangfei_/city-express-api/app/admin/service"
	"gitee.com/zhangfei_/city-express-api/app/admin/service/dto"
	"github.com/gin-gonic/gin"
	"github.com/go-admin-team/go-admin-core/sdk/api"
	"github.com/go-admin-team/go-admin-core/sdk/pkg/jwtauth/user"
)

type {{.EntityName}} struct {
	api.Api
}

// Create{{.EntityName}}
// @Summary {{.EntityTitle}}创建
// @Tags {{.EntityTitle}}
// @Accept  application/json
// @Product application/json
// @Param data body dto.Create{{.EntityName}}Req true " "
// @Success 200 {object} dto.Create{{.EntityName}}Ack
// @Router /api/v1/{{.UrlName}} [post]
// @Security Bearer
func (e {{.EntityName}}) Create{{.EntityName}}(c *gin.Context) {
	var (
		req dto.Create{{.EntityName}}Req
		ack dto.Create{{.EntityName}}Ack
		s   service.{{.EntityName}}
	)
	req.SysUserId = user.GetUserId(c)
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	if ack, err = s.Create{{.EntityName}}(c, req); err != nil {
		e.Error(500, err, "")
		return
	}

	e.OK(ack, "")
}

// Update{{.EntityName}}
// @Summary {{.EntityTitle}}修改
// @Tags {{.EntityTitle}}
// @Accept  application/json
// @Product application/json
// @Param data body dto.Update{{.EntityName}}Req true " "
// @Success 200 {object} dto.Create{{.EntityName}}Ack
// @Router /api/v1/{{.UrlName}} [patch]
// @Security Bearer
func (e {{.EntityName}}) Update{{.EntityName}}(c *gin.Context) {
	var (
		req dto.Update{{.EntityName}}Req
		ack dto.Update{{.EntityName}}Ack
		s   service.{{.EntityName}}
	)
	req.SysUserId = user.GetUserId(c)
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	if ack, err = s.Update{{.EntityName}}(c, req); err != nil {
		e.Error(500, err, "")
		return
	}

	e.OK(ack, "")
}

// Delete{{.EntityName}}
// @Summary {{.EntityTitle}}删除
// @Tags {{.EntityTitle}}
// @Accept  application/json
// @Product application/json
// @Param data body dto.Delete{{.EntityName}}Req true " "
// @Success 200 {object} dto.Delete{{.EntityName}}Ack
// @Router /api/v1/{{.UrlName}} [delete]
// @Security Bearer
func (e {{.EntityName}}) Delete{{.EntityName}}(c *gin.Context) {
	var (
		req dto.Delete{{.EntityName}}Req
		ack dto.Delete{{.EntityName}}Ack
		s   service.{{.EntityName}}
	)
	req.SysUserId = user.GetUserId(c)
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	if ack, err = s.Delete{{.EntityName}}(c, req); err != nil {
		e.Error(500, err, "")
		return
	}

	e.OK(ack, "")
}

// Get{{.EntityName}}
// @Summary {{.EntityTitle}}
// @Tags {{.EntityTitle}}
// @Accept  application/json
// @Product application/json
// @Param data body dto.Get{{.EntityName}}Req true " "
// @Success 200 {object} models.{{.EntityName}}
// @Router /api/v1/{{.UrlName}}/:id [get]
// @Security Bearer
func (e {{.EntityName}}) Get{{.EntityName}}(c *gin.Context) {
	var (
		req dto.Get{{.EntityName}}Req
		ack models.{{.EntityName}}
		s   service.{{.EntityName}}
	)
	req.SysUserId = user.GetUserId(c)
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	if ack, err = s.Get{{.EntityName}}(c, req); err != nil {
		e.Error(500, err, "")
		return
	}

	e.OK(ack, "")
}

// Page{{.EntityName}}
// @Summary {{.EntityTitle}}列表
// @Tags {{.EntityTitle}}
// @Accept  application/json
// @Product application/json
// @Param data body dto.Page{{.EntityName}}Req true " "
// @Success 200 {object} dto.Page{{.EntityName}}Ack
// @Router /api/v1/{{.UrlName}} [get]
// @Security Bearer
func (e {{.EntityName}}) Page{{.EntityName}}(c *gin.Context) {
	var (
		req dto.Page{{.EntityName}}Req
		ack dto.Page{{.EntityName}}Ack
		s   service.{{.EntityName}}
	)
	req.SysUserId = user.GetUserId(c)
	err := e.MakeContext(c).
		MakeOrm().
		Bind(&req).
		MakeService(&s.Service).
		Errors
	if err != nil {
		e.Logger.Error(err)
		e.Error(500, err, err.Error())
		return
	}

	if ack, err = s.Page{{.EntityName}}(c, req); err != nil {
		e.Error(500, err, "")
		return
	}

	e.OK(ack, "")
}
