package t2go

import (
	"fmt"
	"gitee.com/phpdi/cycmd/internal/word"
	"strings"
	"text/template"
)

//注册进tpl的函数

func RegisterFun(tmpl *template.Template) {
	tmpl.Funcs(template.FuncMap{
		"Contains":              Contains,
		"UpperFirstLetter":      UpperFirstLetter,
		"LowerFirstLetter":      LowerFirstLetter,
		"RenderingField":        RenderingField,
		"Join":                  Join,
		"CamelCaseToUnderscore": word.CamelCaseToUnderscore,
		"AddInt":                AddInt,
	})

}

func Contains(ss []string, s string) bool {
	for _, v := range ss {
		if v == s {
			return true
		}
	}
	return false
}

func UpperFirstLetter(str string) string {
	if len(str) == 0 {
		return str
	}

	return strings.ToUpper(string(str[0])) + str[1:]
}

func LowerFirstLetter(str string) string {
	if len(str) == 0 {
		return str
	}

	return strings.ToLower(string(str[0])) + str[1:]
}

// RenderingField 渲染字段
func RenderingField(f Field, jsonTag, gormTag bool) string {
	tag := ""
	if jsonTag {
		tag += f.JsonTag + " "
	}

	if gormTag {
		tag += f.GormTag + " "
	}

	tag = strings.TrimRight(tag, " ")
	if tag != "" {
		return fmt.Sprintf("%s %s `%s` // %s", f.Key, f.GoType, tag, f.Comment.Comment)
	}

	return fmt.Sprintf("%s %s // %s", f.Key, f.GoType, f.Comment.Comment)
}

func Join(s []string) string {
	return strings.Join(s, ",")
}

func AddInt(a, b int) int {
	return a + b
}
