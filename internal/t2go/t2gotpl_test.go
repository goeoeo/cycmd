package t2go

import (
	"log"
	"testing"
)

func TestT2GoTpl_Run(t *testing.T) {
	ti := NewMysqlTableInfo("root:2DnqzOVB0WGN@(139.198.123.116:33306)/goadmin?charset=utf8")
	tgt := NewT2GoTpl(ti, &Config{
		TableName: "exp_coupon",
		WorkDir:   "template",
	})
	err := tgt.Run()
	if err != nil {
		log.Fatal(err)
	}
}

func TestT2GoTpl_templateRendering(t *testing.T) {
	ti := NewMysqlTableInfo("root:2DnqzOVB0WGN@(139.198.123.116:33306)/goadmin?charset=utf8")
	tgt := NewT2GoTpl(ti, &Config{
		TableName: "exp_commission_trans",
		WorkDir:   "template",
	})
	if err := tgt.tableToTemplateData(); err != nil {
		log.Fatal(err)
	}
	//util.PrintJson(tgt.td)
	//if err := tgt.templateRendering("template/models/model.tpl", "model.tpl"); err != nil {
	//	log.Fatal(err)
	//}

	//if err := tgt.templateRendering("template/service/dto/dto.tpl", "dto.tpl"); err != nil {
	//	log.Fatal(err)
	//}
	//
	//if err := tgt.templateRendering("template/table.tpl", "table.tpl"); err != nil {
	//	log.Fatal(err)
	//}

	tgt.cfg.Suffix = ".proto"
	if err := tgt.templateRendering("template/proto/proto.tpl", "proto.tpl"); err != nil {
		log.Fatal(err)
	}
}

func TestT2GoTpl_Clear(t *testing.T) {
	tgt := NewT2GoTpl(nil, &Config{
		TableName: "exp_invitation",
		WorkDir:   "template",
	})
	err := tgt.Clear()
	if err != nil {
		log.Fatal(err)
	}
}
