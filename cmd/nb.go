package cmd

import (
	"gitee.com/phpdi/cycmd/internal/nb"
	"github.com/spf13/cobra"
)

var (
	nbDns string

	nbCmd = &cobra.Command{
		Use:   "nb",
		Short: "",
		Run: func(cmd *cobra.Command, args []string) {
			nb.Run(nbDns)
		},
	}
)

func init() {
	nbCmd.Flags().StringVarP(&nbDns, "nbDns", "d", "", "数据库连接信息（root:123456@tcp(192.168.49.2:32316)/pricing?charset=utf8）")
}
